﻿using CTSites.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Data;
using System.Net.Mail;
using System.Text;

namespace CTSites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        public OrdersController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GetOrders")]
        public string GetOrders()
        {
            SqlConnection conn=new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("spGetAllOrders", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt=new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        [Route("CreateOrder")]
        [HttpPost]
        public List<tblOrders> CreateOrders(tblOrders Order)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spCreateOrder";
            var result = new List<tblOrders>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Siteid", Order.Siteid);
                command.Parameters.AddWithValue("@Userid", Order.Userid);
                command.Parameters.AddWithValue("@CTOrderNumber", Order.CTOrderNumber);
                command.Parameters.AddWithValue("@CTPONumber", Order.CTPONumber);
                command.Parameters.AddWithValue("@CTOrderDate", Order.CTOrderDate);
                command.Parameters.AddWithValue("@StatusId", Order.StatusId);
                command.Parameters.AddWithValue("@EnteredBy", Order.EnteredBy);
                command.Parameters.AddWithValue("@DateEntered", Order.DateEntered);
                command.Parameters.AddWithValue("@OrderType", Order.OrderType);
                command.Parameters.AddWithValue("@CTShipToInformation_AccountNumber", Order.CTShipToInformation_AccountNumber);
                command.Parameters.AddWithValue("@CTShipToInformation_CompanyName", Order.CTShipToInformation_CompanyName);
                command.Parameters.AddWithValue("@CTShipToInformation_Name", Order.CTShipToInformation_Name);
                command.Parameters.AddWithValue("@CTShipToInformation_City", Order.CTShipToInformation_City);
                command.Parameters.AddWithValue("@CTShipToInformation_State", Order.CTShipToInformation_State);
                command.Parameters.AddWithValue("@CTShipToInformation_ZipCode", Order.CTShipToInformation_ZipCode);
                command.Parameters.AddWithValue("@CTShipToInformation_Country", Order.CTShipToInformation_Country);
                command.Parameters.AddWithValue("@CTShipToInformation_Phone", Order.CTShipToInformation_Phone);
                command.Parameters.AddWithValue("@CTShipToInformation_EmailAddress", Order.CTShipToInformation_EmailAddress);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        
                            Order.Siteid = int.Parse(reader["Siteid"].ToString());
                            Order.Userid = int.Parse(reader["Userid"].ToString());
                        Order.CTOrderNumber = reader["CTOrderNumber"].ToString();
                            Order.CTPONumber = reader["CTPONumber"].ToString();
                            Order.CTOrderDate =DateTime.Parse(reader["CTOrderDate"].ToString());
                            Order.AnticipatedShipDate = DateTime.Parse(reader["AnticipatedShipDate"].ToString());
                        Order.StatusId = int.Parse(reader["StatusId"].ToString());
                            Order.EnteredBy = reader["EnteredBy"].ToString();
                            Order.DateEntered = DateTime.Parse(reader["DateEntered"].ToString());
                        Order.DateUpdated = DateTime.Parse(reader["DateUpdated"].ToString());
                        Order.OrderType = reader["OrderType"].ToString();
                            Order.CTShipToInformation_AccountNumber = reader["CTShipToInformation_AccountNumber"].ToString();
                            Order.CTShipToInformation_CompanyName = reader["CTShipToInformation_CompanyName"].ToString();
                            Order.CTShipToInformation_Name = reader["CTShipToInformation_Name"].ToString();
                            Order.CTShipToInformation_Address = reader["CTShipToInformation_Address"].ToString();
                            Order.CTShipToInformation_City = reader["CTShipToInformation_City"].ToString();
                            Order.CTShipToInformation_State = reader["CTShipToInformation_State"].ToString();
                            Order.CTShipToInformation_ZipCode = reader["CTShipToInformation_ZipCode"].ToString();
                            Order.CTShipToInformation_Country = reader["CTShipToInformation_Country"].ToString();
                            Order.CTShipToInformation_Phone = reader["CTShipToInformation_Phone"].ToString();
                            Order.CTShipToInformation_EmailAddress = reader["CTShipToInformation_EmailAddress"].ToString();
                            Order.TerritoryNumber = reader["TerritoryNumber"].ToString();
                            Order.RegionNumber = reader["RegionNumber"].ToString();
                            Order.ArtworkID = reader["ArtworkID"].ToString();
                            Order.ProofID = reader["ProofID"].ToString();
                            Order.RepeatOrderComment = reader["RepeatOrderComment"].ToString();

                            tblOrders tmpRecord = new tblOrders()
                            {
                            Siteid = Order.Siteid,
Userid = Order.Userid,
CTOrderNumber  = Order.CTOrderNumber ,
CTPONumber  = Order.CTPONumber ,
CTOrderDate  = Order.CTOrderDate ,
AnticipatedShipDate = Order.AnticipatedShipDate,
StatusId  = Order.StatusId ,
EnteredBy  = Order.EnteredBy ,
DateEntered  = Order.DateEntered ,
DateUpdated  = Order.DateUpdated ,
OrderType = Order.OrderType,
CTShipToInformation_AccountNumber  = Order.CTShipToInformation_AccountNumber ,
CTShipToInformation_CompanyName  = Order.CTShipToInformation_CompanyName ,
CTShipToInformation_Name = Order.CTShipToInformation_Name,
CTShipToInformation_Address  = Order.CTShipToInformation_Address ,
CTShipToInformation_City  = Order.CTShipToInformation_City ,
CTShipToInformation_State = Order.CTShipToInformation_State,
CTShipToInformation_ZipCode  = Order.CTShipToInformation_ZipCode ,
CTShipToInformation_Country = Order.CTShipToInformation_Country,
CTShipToInformation_Phone = Order.CTShipToInformation_Phone,
CTShipToInformation_EmailAddress  = Order.CTShipToInformation_EmailAddress ,
TerritoryNumber  = Order.TerritoryNumber ,
RegionNumber = Order.RegionNumber,
ArtworkID  = Order.ArtworkID ,
ProofID = Order.ProofID,
RepeatOrderComment = Order.RepeatOrderComment,

                            };
                            result.Add(tmpRecord);
                       // }
                    }
                }
                con.Close();
            }
            return result;
        }

        [Route("InsertProductDetails")]
        [HttpPost]
        public List<tblOrderDetails> InserProductDetails(tblOrderDetails Order)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spCreateOrder";
            var result = new List<tblOrderDetails>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", Order.OrderID);
                command.Parameters.AddWithValue("@ProductCode", Order.ProductCode);
                command.Parameters.AddWithValue("@ProductDescription", Order.ProductDescription);
                command.Parameters.AddWithValue("@ProductQty", Order.ProductQty);
                command.Parameters.AddWithValue("@ReferenceNo", Order.ReferenceNo);
                
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        Order.OrderID = reader["Siteid"].ToString();
                        Order.ProductCode = reader["Userid"].ToString();
                        Order.ProductDescription = reader["CTOrderNumber"].ToString();
                        Order.ProductQty = reader["CTPONumber"].ToString();
                        Order.ReferenceNo = reader["ReferenceNo"].ToString();


                        tblOrderDetails tmpRecord = new tblOrderDetails()
                        {
                            OrderID = Order.OrderID,
                            ProductCode = Order.ProductCode,
                            ProductDescription = Order.ProductDescription,
                            ProductQty = Order.ProductQty,
                            ReferenceNo = Order.ReferenceNo
                           

                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }

            [Route("InsertUpdateCustomerEmails")]
        [HttpPost]
        public List<tblCustomerEmails> InsertUpdateCustomerEmails(tblCustomerEmails Customer)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertUpdateCustomerEmails";
            var result = new List<tblCustomerEmails>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", Customer.OrderID);
                command.Parameters.AddWithValue("@AssignedUserid", Customer.Userid);
                command.Parameters.AddWithValue("@OrderByNo", Customer.OrderByAccountNumber);
                command.Parameters.AddWithValue("@ShipToNo", Customer.ShipToAccountNumber);
                command.Parameters.AddWithValue("@FullName", Customer.FullName);
                command.Parameters.AddWithValue("@CustEmail", Customer.EmailAddress);
                command.Parameters.AddWithValue("@Userid", Customer.EnteredBy);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        Customer.OrderID = reader["OrderID"].ToString();
                        Customer.Userid = reader["Userid"].ToString();
                        Customer.OrderByAccountNumber = reader["OrderByAccountNumber"].ToString();
                        Customer.ShipToAccountNumber = reader["ShipToAccountNumber"].ToString();
                        Customer.EnteredBy = reader["EnteredBy"].ToString();


                        tblCustomerEmails tmpRecord = new tblCustomerEmails()
                        {
                            OrderID = Customer.OrderID,
                            Userid = Customer.Userid,
                            OrderByAccountNumber = Customer.OrderByAccountNumber,
                            ShipToAccountNumber = Customer.ShipToAccountNumber,
                            EnteredBy = Customer.EnteredBy
                           

                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }

        [Route("InsertArtwork")]
        [HttpPost]
        public List<tblArtwork> spInsertArtwork(tblArtwork artwork)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertArtwork";
            var result = new List<tblArtwork>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserId", artwork.UserId);
                command.Parameters.AddWithValue("@FileName", artwork.FileName);
                command.Parameters.AddWithValue("@OrigFileName", artwork.OrigFileName);
                command.Parameters.AddWithValue("@DateAdded", artwork.DateAdded);
                command.Parameters.AddWithValue("@PMSColors", artwork.PMSColors);
            

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        artwork.UserId = reader["OrderID"].ToString();
                        artwork.FileName = reader["Userid"].ToString();
                        artwork.OrigFileName = reader["OrderByAccountNumber"].ToString();
                        artwork.DateAdded = reader["ShipToAccountNumber"].ToString();
                        artwork.PMSColors = reader["EnteredBy"].ToString();


                        tblArtwork tmpRecord = new tblArtwork()
                        {
                            UserId = artwork.UserId,
                            FileName = artwork.FileName,
                            OrigFileName = artwork.OrigFileName,
                            DateAdded = artwork.DateAdded,
                            PMSColors = artwork.PMSColors


                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }

        [Route("InsertProof")]
        [HttpPost]
        public List<tblProofs> spInsertProof(tblProofs proof)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertProof";
            var result = new List<tblProofs>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserId", proof.UserId);
                command.Parameters.AddWithValue("@UserId", proof.UserId);
                command.Parameters.AddWithValue("@OrigFileName", proof.OrigFileName);
                command.Parameters.AddWithValue("@DateAdded", proof.DateAdded);
                command.Parameters.AddWithValue("@PMSColors", proof.PMSColors);
                command.Parameters.AddWithValue("@CustomerComments", proof.CustomerComments);
                command.Parameters.AddWithValue("@CustomerStatus", proof.CustomerStatus);


                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        proof.UserId = reader["OrderID"].ToString();
                        proof.FileName = reader["Userid"].ToString();
                        proof.OrigFileName = reader["OrderByAccountNumber"].ToString();
                        proof.DateAdded = reader["ShipToAccountNumber"].ToString();
                        proof.PMSColors = reader["EnteredBy"].ToString();


                        tblProofs tmpRecord = new tblProofs()
                        {
                            UserId = proof.UserId,
                            FileName = proof.FileName,
                            OrigFileName = proof.OrigFileName,
                            DateAdded = proof.DateAdded,
                            PMSColors = proof.PMSColors


                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }


        [HttpPost]
        public JsonResult Sendmail([FromBody] string FirstName, string LastName, string subject, string body,string Email)
        {
            string Message = "";

        
            MailMessage msg = new MailMessage();
            SmtpClient client = new SmtpClient();
            msg.From = new MailAddress("DoNotReply@saint-gobain.com", "No-Reply", System.Text.Encoding.UTF8);
            msg.Subject = subject;//Message header 
            msg.SubjectEncoding = Encoding.UTF8;//Mail title code 
            msg.IsBodyHtml = true;

            msg.Body = body;
            msg.To.Add(Email);
            msg.BodyEncoding = Encoding.UTF8;//Message code 
            msg.IsBodyHtml = true;//Whether the HTML mail 
            msg.Priority = MailPriority.High;//Priority mail

            // SmtpServer.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("", "password");
            //client.Host = Get_AppSettings("smtpKey");
            client.Host = "localhost";
            object userState = msg;
            try
            {
                client.Send(msg);
                Message = "success";
                //  return true;
            }
            catch (Exception err)
            {
                Message = err.InnerException.ToString();
                System.Diagnostics.Debug.WriteLine("MAIL ERROR: " + err.Message.ToString());
            }

            return new JsonResult(Message);
        }


    }
}
