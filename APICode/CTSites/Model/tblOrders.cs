﻿namespace CTSites.Model
{
    public class tblOrders
    {
        public int ProxyID { get; set; }
        public string OrderID { get; set; }
        public int Userid { get; set; }
        public int Siteid { get; set; }
        public string CTOrderNumber { get; set; }
        public DateTime CTOrderDate { get; set; }
        public string CTPONumber { get; set; }
        public string CTShipToInformation_AccountNumber { get; set; }
        public string CTShipToInformation_Name { get; set; }
        public string CTShipToInformation_CompanyName { get; set; }
        public string CTShipToInformation_Address { get; set; }
        public string CTShipToInformation_City { get; set; }
        public string CTShipToInformation_ZipCode { get; set; }
        public string CTShipToInformation_State { get; set; }
        public string CTShipToInformation_Country { get; set; }
        public string CTShipToInformation_EmailAddress { get; set; }
        public string CTShipToInformation_Phone { get; set; }
        public string ArtworkID { get; set; }
        public string ProofID { get; set; }
        public int StatusId { get; set; }
        public string EnteredBy { get; set; }
        public DateTime DateEntered { get; set; }
        public DateTime DateUpdated { get; set; }
        public string ApprovedByClient { get; set; }
        public string TerritoryNumber { get; set; }
        public string RegionNumber { get; set; }
        public DateTime AnticipatedShipDate { get; set; }
        public DateTime ActualShipDate { get; set; }
        public string OrderType { get; set; }
        public string RepeatOrderComment { get; set; }

    }
}
