﻿namespace CTSites.Model
{
    public class tblOrderDetails
    {
        public int ProxyID { get; set; }
        public string OrderID { get; set; }
        public string ProductCode { get; set; }
        public string ProductDescription { get; set; }
        public string ProductQty { get; set; }
        public string ReferenceNo { get; set; }

   
    }
}
