import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { EmailverifyComponent } from './components/emailverify/emailverify.component';
import { VerifyotpComponent } from './components/verifyotp/verifyotp.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LandingComponent } from './components/landing/landing.component';
import { SsoComponent } from './components/sso/sso.component';
import { AuthorizeComponent } from './components/authorize/authorize.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { CreateNewOrderComponent } from './components/create-new-order/create-new-order.component';
import { RepeatOrderComponent } from './components/repeat-order/repeat-order.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { RoleAccessComponent } from './components/role-access/role-access.component';
import { ViewOrderComponent } from './components/view-order/view-order.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AllOrdersComponent } from './components/all-orders/all-orders.component';
import { UploadProofComponent } from './components/upload-proof/upload-proof.component';
import { ReviewProofComponent } from './components/review-proof/review-proof.component';

const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'verifyemail', component: EmailverifyComponent },
  { path: 'verifyotp', component: VerifyotpComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'sso', component: SsoComponent },
  { path: 'authorize', component: AuthorizeComponent },
  { path: 'userprofile', component: UserprofileComponent },
  { path: 'createNewOrder', component: CreateNewOrderComponent },
  { path: 'repeatOrder', component: RepeatOrderComponent },
  { path: 'createUser', component: CreateUserComponent },
  { path: 'roleAccess', component: RoleAccessComponent },
  { path: 'viewOrder', component: ViewOrderComponent},
  { path: 'reports', component: ReportsComponent },
  { path: 'allOrders', component: AllOrdersComponent },
  { path: 'uploadProof', component: UploadProofComponent },
  { path: 'reviewProof', component: ReviewProofComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
