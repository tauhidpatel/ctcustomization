import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { EmailverifyComponent } from './components/emailverify/emailverify.component';
import { VerifyotpComponent } from './components/verifyotp/verifyotp.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LandingComponent } from './components/landing/landing.component';
import { SsoComponent } from './components/sso/sso.component';
import { AuthorizeComponent } from './components/authorize/authorize.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { CreateNewOrderComponent } from './components/create-new-order/create-new-order.component';
import { RepeatOrderComponent } from './components/repeat-order/repeat-order.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { RoleAccessComponent } from './components/role-access/role-access.component';
import { ViewOrderComponent } from './components/view-order/view-order.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AllOrdersComponent } from './components/all-orders/all-orders.component';
import { UploadProofComponent } from './components/upload-proof/upload-proof.component';
import { ReviewProofComponent } from './components/review-proof/review-proof.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    EmailverifyComponent,
    VerifyotpComponent,
    DashboardComponent,
    WelcomeComponent,
    LandingComponent,
    SsoComponent,
    AuthorizeComponent,
    UserprofileComponent,
    CreateNewOrderComponent,
    RepeatOrderComponent,
    CreateUserComponent,
    RoleAccessComponent,
    ViewOrderComponent,
    ReportsComponent,
    AllOrdersComponent,
    UploadProofComponent,
    ReviewProofComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', '.json');
        },
        deps: [HttpClient]  
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
