import { Component } from '@angular/core';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  userId: any;
  SGID!: string;
  Name!: string;

  constructor() {
    this.userId = localStorage.getItem("okta-cache-storage");
    this.SGID = sessionStorage.getItem("SGID") ?? '';
    this.Name = sessionStorage.getItem("Name") ?? '';
    console.log(this.SGID);
  }

  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('#example').DataTable({
        paging: false,
        searching: false,
        info :false
      });
    });

    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

  

  
}
