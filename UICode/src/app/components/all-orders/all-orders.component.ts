import { Component } from '@angular/core';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent {
  userId: any;
  SGID!: string;
  Name!: string;

  constructor() {
    this.userId = localStorage.getItem("okta-cache-storage");
    this.SGID = sessionStorage.getItem("SGID") ?? '';
    this.Name = sessionStorage.getItem("Name") ?? '';
    console.log(this.SGID);
  }

  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('#example').DataTable({
        // paging: false,
        // searching: false,
        // info: false
      });
    });

    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

}
