import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.css']
})
export class AuthorizeComponent {

  pathPrefix:any;
  URL:any;

  constructor( private route: ActivatedRoute,private _global: GlobalService) { 
    this.URL=_global.URL;
    this.pathPrefix=_global.pathPrefix;
      }

  ngOnInit() {
  
    this.route.queryParams
    .subscribe(params => {
      console.log(params);
      
      //this.orderby=params["SGID"]; 
     // this.Authenticate(this.orderby);
     sessionStorage.setItem("SGID",params["SGID"]);
     sessionStorage.setItem("Name",params["Name"]);
    // window.location.href = "https://digital-team-825wb.certainteed.com/weather/dashboard" ;
   // window.location.href = "http://"+this.pathPrefix+"/dashboard" ;;
    window.location.href = "http://localhost:4200/welcome";
    //window.location.href = "https://weather.certainteed.com/weather/dashboard"
     });
    //  console.log(this.orderby);

    }
  }
