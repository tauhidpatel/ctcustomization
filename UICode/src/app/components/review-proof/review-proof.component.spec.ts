import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewProofComponent } from './review-proof.component';

describe('ReviewProofComponent', () => {
  let component: ReviewProofComponent;
  let fixture: ComponentFixture<ReviewProofComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewProofComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviewProofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
