import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Okta } from 'src/app/shared/okta/okta.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  showIcons: boolean = true;
  showSuperRoles: boolean = false;
  user: any;
  oktaSignIn: any;

  constructor(
    private okta: Okta, 
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private zone: NgZone,
    private http: HttpClient
  ) 
  {
    this.oktaSignIn = okta.getWidget();
    // debugger;
  } 

  showLogin(): void {
    this.oktaSignIn?.remove();
    this.oktaSignIn.renderEl({el: '#okta-login-container'}, (response: any) => {
      if (response.status === 'SUCCESS') {
        this.oktaSignIn.authClient.tokenManager.add('idToken', response.tokens.idToken);
        this.oktaSignIn.authClient.tokenManager.add('accessToken', response.tokens.accessToken);
        this.user = response.tokens.idToken.claims.email;
        sessionStorage.setItem("userId",response.tokens.idToken.claims.sub);
        this.oktaSignIn.remove();
        this.changeDetectorRef.detectChanges();
        this.zone.run(() => {
          this.router.navigate(['/dashboard']); 
      });
    } 
    });
  }
  

  // async ngOnInit(): Promise<void> {
  //   try {
  //     this.user = await this.oktaSignIn.authClient.token.getUserInfo();
  //     console.log(this.oktaSignIn.authClient.token.getUserInfo());
  //     console.log("helllllllllllooooooooooooo")
  //   } catch (error) {
  //     this.showLogin();
  //   }
  // }

  async ngOnInit(): Promise<void> {
   
  }
  async ngAfterViewInit() {
    try {
      var response = await this.oktaSignIn.authClient.session.get();
      // console.log(response);
      if (response.status !== "INACTIVE") {
        sessionStorage.setItem("userId",response.userId);
        this.router.navigate(['/dashboard']); 
      }
      else
      {
        this.showLogin();
      }
    } catch (error) {
    }
  }


}
