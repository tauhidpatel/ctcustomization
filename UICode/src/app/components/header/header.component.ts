import { Component, ElementRef, HostListener, Inject, LOCALE_ID } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Okta } from 'src/app/shared/okta/okta.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
declare var bootstrap: any;
import * as $ from "jquery";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  Name!: string;
  showWelcome: any;
  showLogoutButton: boolean = true;
  showNavItems: boolean = true;
  showIcons: boolean = true;
  oktaSignIn: any;
  private destroy$ = new Subject<void>();

  // FLAG MENU VARIABLES
  isDropdownOpen: boolean = false;
  mySubscription;
  myDate = new Date();
  isMenuOpened: boolean = false;
  locale: any;
  flag: any;
  langauge: any;

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    var target = event.target;
    if(target.parentElement.classList[1]=='utility-drop__trigger') {
      if($(".dropdown--country").hasClass("open"))
      {
        $(".dropdown--country").removeClass("open");
      }
      else
      {
      $(".dropdown--country").addClass("open");
      }
    } else {
      $(".dropdown--country").removeClass("open");
    }

    // Check if the clicked element is part of the dropdown trigger
    if (target.classList.contains('utility-drop__trigger') || target.closest('.utility-drop__trigger')) {
      this.toggleDropdown(event);
    } else {
      this.closeDropdown();
    }

    // Check if the clicked element is part of the dropdown trigger
    // if (target.classList.contains('utility-drop__trigger')) {
    //   this.toggleDropdown();
    // } else {
    //   this.closeDropdown();
    // }
  }

  ngOnInit(): void {

    this.flag = sessionStorage.getItem('flag');
    this.langauge=sessionStorage.getItem('lang');  
    if(sessionStorage.getItem('flag')) {
      if(sessionStorage.getItem('flag')=='cn') {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-cn');
      }
      else if(sessionStorage.getItem('flag')=='us') {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-us');
      }
    }

    this.router.events
      .pipe(takeUntil(this.destroy$))
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.showIcons = this.shouldShowIcons();
          this.showLogoutButton = this.shouldShowLogoutButton();
          this.showNavItems = this.shouldShowNavItems();
          this.showWelcome = this.shouldShowWelcome();
        }
      });
  }

  ngAfterViewInit(): void {
    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

  constructor(
    private router: Router, 
    private okta: Okta,
    private translate: TranslateService,
    private eRef: ElementRef,
    @Inject(LOCALE_ID) locale: string
    ) { 
      this.Name = sessionStorage.getItem("Name") ?? '';
      // debugger;
      if (sessionStorage.getItem('lang')) {
        console.log('getting old lang');
        console.log(sessionStorage.getItem('lang'));
        this.locale = sessionStorage.getItem('lang');
      } else {
        this.locale = navigator.language;
    
        if(this.locale=='en-US')
        {
          environment.language="en";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
        sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-419')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-mx')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-us')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='fr-CA')
        {
        
          environment.language="fr";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "fr");
          sessionStorage.setItem('flag', "cn");
         // document.title = "Centre client | CertainTeed";
        }
        else  if(this.locale=='fr-fr')
        {
        
          environment.language="fr";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "fr");
          sessionStorage.setItem('flag', "cn");
         // document.title = "Centre client | CertainTeed";
        }
        else  if(this.locale=='en-CA')
        {
          environment.language="en";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
          sessionStorage.setItem('flag', "cn");
        }
        else
        {
          environment.language="en";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
          sessionStorage.setItem('flag', "us");
        }
        // alert(this.flag);
      }
      this.translate.use(sessionStorage.getItem("lang") ?? '');
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.mySubscription = this.router.events.subscribe((event) => {
          if (event instanceof NavigationEnd) {
      // Trick the Router into believing it's last link wasn't previously loaded
          this.router.navigated = false;
          }
        }); 
    }

    toggleDropdown(event: Event) {
      event.stopPropagation(); // Prevent the click event from reaching the document click handler
      this.isDropdownOpen = !this.isDropdownOpen;
    }

    closeDropdown() {
      this.isDropdownOpen = false;
    }

    useLanguage(language: string,flag:string) {
      sessionStorage.setItem('lang', language);
      sessionStorage.setItem('flag', flag);
      this.flag=flag;
      this.langauge=language;
      if(flag=='cn')
      {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-cn');
      }
     else if(flag=='us')
      {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-us');
      }
    
    
      this.translate.use(language);
      document.querySelector('html')?.setAttribute('lang', language);
      environment.language=language;
         if(language=='fr')
      {
         document.title = "Centre client | CertainTeed";
      }
      window.location.reload();
     // const currentUrl = this.router.url;
     // this.router.navigate([currentUrl])
      debugger;
    }

    open(i: any) {
      const triggerElement = $(".utility-drop__trigger")[i];
      if (triggerElement) {
        const parentElement = triggerElement.parentElement as HTMLElement;
        if (parentElement) {
          parentElement.classList.toggle("open");
        }
      }
    }

    ngOnDestroy() {
      if (this.mySubscription) {
       this.mySubscription.unsubscribe();
      }
    }
    
  shouldShowWelcome(): any {
    const currentRoute = this.router.url;
    const routesWithoutWelcome = ['landing'];

    return !routesWithoutWelcome.some(route => currentRoute.endsWith(route));
  }  

  shouldShowLogoutButton(): boolean {
    const currentRoute = this.router.url;
    const routesWithoutLogoutButton = ['landing', '/login'];
  
    return !routesWithoutLogoutButton.some(route => currentRoute.endsWith(route));
  }

  shouldShowNavItems(): boolean {
    const currentRoute = this.router.url;
    const routesWithoutNavItems = [ 'landing', 'login' ];

    return !routesWithoutNavItems.some(route => currentRoute.endsWith(route));
  }

  shouldShowIcons(): boolean {
    const currentRoute = this.router.url;
    const restrictedRoutes = [ '/login', '/landing' ];
  
    return !restrictedRoutes.some(route => currentRoute.endsWith(route));
  }
  

  logout(): void {
    this.oktaSignIn = this.okta.getWidget();

    this.oktaSignIn.authClient.signOut({
      clearTokensBeforeRedirect: true,
      postLogoutRedirectUri: 'http://localhost:4200'
    })

    sessionStorage.removeItem('SGID');
    sessionStorage.clear();
    localStorage.clear();
  }

}
