import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare var $: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {

  showSuperRoles: boolean = true;
  Name!: string;

  supportLanguages = ['en', 'fr', 'es'];

  constructor(private translateService: TranslateService) {
    this.Name = sessionStorage.getItem("Name") ?? '';

    this.translateService.addLangs(this.supportLanguages);
    this.translateService.setDefaultLang('en')
  }

  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('#example').DataTable();
    });
  }

}
