import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { SsoService } from 'src/app/services/sso.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent {

  showWelcome: any = false;
  showIcons: boolean = true;
  showSuperRoles: boolean = false;
  
  loginSuccessMessage!: string;
  SGID: string | undefined;
  URl: any;
  pathPrefix: any;

  constructor(
    private http: HttpClient,
    private ssoService: SsoService, 
    private router: Router, 
    private route: ActivatedRoute,
    private _global: GlobalService) 
  { 
    this.URl = _global.URL;
    this.pathPrefix = _global.pathPrefix;
  }

  // ngOnInit(): void {
  //   this.checkSsoTicket();
  // }
  
  navigateToSsoPage() {
    // debugger;
    // Redirect to the SSO login page
    window.location.href = "https://uat.websso.saint-gobain.com/cas/login" + "?service=" + "http://localhost:54100/" + "api/sso1/";
  }

   
    // this.http.get<any>('http://localhost:54100/api/sso1').subscribe(
    //   (response) => {
    //     // Assuming response is { SGID: "your_sgid_value" }
    //     const sgid = response.SGID;
    //     // Store SGID in the browser (e.g., using sessionStorage)
    //     sessionStorage.setItem('SGID', sgid);
    //     // Perform additional actions if needed
    //   },
    //   (error) => {
    //     console.error('SSO request failed:', error);
    //     // Handle error as needed
    //   }
    // );
  

//   handleSsoResponse(ticket: string) {
//     // Call the SSO service to authenticate
//     this.ssoService.authenticate(ticket).subscribe(
//         (sgid: string) => {
//             // Store SGID in SessionStorage
//             sessionStorage.setItem('SGID', sgid);

//             // Redirect to the dashboard page
//             this.router.navigate(['/dashboard']);
//         },
//         (error) => {
//             // Handle error if needed
//             console.error('SSO request failed:', error);
//         }
//     );
// }

  // ngOnInit(): void {
  //   this.navigateToSsoPage();
  // }


  // navigateToSsoPage() {
  //   debugger;
  //   this.SGID = sessionStorage.getItem("SGID")?.toString();

  //   if(this.SGID !== undefined) {
  //    console.log(this.pathPrefix);
  //   } else if ( this.SGID == "" ) {
  //     window.location.href = "http://"+this.pathPrefix+"/landing";
  //     sessionStorage.removeItem('SGID');
  //     sessionStorage.removeItem('Name');
  //   } else {
  //     window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "http://localhost:54100/" +"api/sso1/";

  //   }
  // }
  //navigateToSsoPage() {
    // this.router.navigate(['/sso']);
    //window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + 'http://localhost:54100/' + "api/sso1" ;
    // https://localhost:7049
    // http://localhost:54100/
  //}

  // ngOnInit(): void {
  //   const urlParams = new URLSearchParams(window.location.search);
  //   if (urlParams.has('ticket')) {
  //     this.handleSsoCallback();
  //   }
  // }

  // navigateToSsoPage(): void {
  //   this.ssoService.navigateToSsoPage();
  // }

  // handleSsoCallback(): void {
  //   this.ssoService.handleSsoCallback().subscribe(
  //     sgid => {
  //       this.ssoService.storeSgidInSession(sgid);
  //       // Redirect to the dashboard or any other page
  //       // You may use Angular Router for navigation
  //       window.location.href = '/dashboard';
  //     },
  //     error => {
  //       console.error('Error handling SSO callback:', error);
  //     }
  //   );
  // }
}
